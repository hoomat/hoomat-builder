<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="css/materialize-rtl.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Hoomat ES Builder - Add New Question</title>
  </head>

  <body>

    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
            <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section id="movie-info" class="container mt-5">
        <h5 class="white-text flashit mb-5">افزودن سوال</h5>
        <div class="row">
            <form action="add_question.php" method="POST">
                <div class="input-field col s6">
                    <select name="type" id="type">
                        <option value="" disabled selected>نوع شاخص را انتخاب کنید</option>
                        <option value="varchar">VARCHAR</option>
                        <option value="integer">INT</option>
                        <option value="text">TEXT</option>
                        <option value="float">FLOAT</option>
                        <option value="multiple">MULTIPLE</option>
                        <option value="image">IMAGE</option>
                    </select>
                    <label for="type">نوع شاخص</label>
                </div>

                <div class="input-field col s6">
                    <input id="value" name="value" type="text">
                    <label for="value">عنوان شاخص (انگلیسی)</label>
                </div>

                <div class="input-field col s12">
                    <input id="name" name="name" type="text">
                    <label for="name">عنوان شاخص (فارسی)</label>
                </div>

                <div class="input-field col s12">
                    <input id="question" name="question" type="text">
                    <label for="question">متن سوال</label>
                </div>

                <button name="form-question" id="form-question" type="submit" class="waves-effect waves-light my-md-font btn-large yellow accent-4 black-text float-left m-3">ثبت</button>
            </form>
        </div>
        
    </section>

            
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script>
        $(document).ready(function() {
        $('select').material_select();
        });

        
        $(document).ready(function(){
        $('.modal').modal();
        });

        $(".button-collapse").sideNav();

        function openNav() {
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("main").style.marginLeft = "200px";
        }

        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        }
    
    </script>
  </body>
</html>
