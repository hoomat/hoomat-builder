<?php
require_once 'DB.php';
$db = DB::getInstance();
if (isset($_POST['form-conclusion'])) {
    $sql = "SELECT * FROM indexes";
    $indexes = $db->query($sql)->results();
    $fields = [];

    foreach ($indexes as $index) {
        if ($index->type == 'image'){
            $target_dir = "/uploads/";
            $target_file = $target_dir . basename($_FILES[$index->value]["name"]);
            
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // $check = getimagesize($_FILES[$index->value]["tmp_name"]);
            // if($check !== false) {
            //     $uploadOk = 1;
            // } else {
            //     header("location: questions.php");
            //     $uploadOk = 0;
            // }

            if (file_exists($target_file)) {
                $uploadOk = 0;
            }

            if ($_FILES[$index->value]["size"] > 20097152) {
                $uploadOk = 0;
            }

            if ($uploadOk === 1) {
                move_uploaded_file($_FILES[$index->value]["tmp_name"], "uploads/".$_FILES[$index->value]["name"]);
                $fields[$index->value] = "uploads/".$_FILES[$index->value]["name"];
            }
        } else {
            $fields[$index->value] = trim(htmlspecialchars($_POST[$index->value]));
        }
    }

    if (empty($fields)) {
        header("location: questions.php");
    } else {
        if ($db->insert('conclusion', $fields)) {
            header("location: questions.php");
        }
    }
} else {
    header("location: questions.php");
}