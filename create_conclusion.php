<?php
require_once 'DB.php';
$db = DB::getInstance();

$sql = "SELECT * FROM indexes";
$indexes = $db->query($sql)->results();
?>
<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="css/materialize-rtl.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <title>Hoomat ES Builder - Add New Conclusion</title>

    <style>
        .row .col {
            float: right !important;
        }
    </style>
  </head>

  <body>

    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
            <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section id="movie-info" class="container mt-5">
        <h5 class="white-text flashit mb-3">مشخصات فیلم</h5>
        <div class="row">
            <form action="add_conclusion.php" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php foreach ($indexes as $index) { ?>
                        <div class="input-field col s6">
                            <?php if ($index->type === 'varchar' || $index->type === 'float' || $index->type === 'multiple') { ?>
                            <input type="text" name="<?php echo $index->value; ?>" id="<?php echo $index->value; ?>">
                            
                            <?php } elseif ($index->type === 'integer') { ?>
                            <input type="number" name="<?php echo $index->value; ?>" id="<?php echo $index->value; ?>">
                            
                            <?php } elseif ($index->type === 'text') { ?>
                             <div class="input-field col s12">
                                <textarea rows="6" name="<?php echo $index->value; ?>" id="<?php echo $index->value; ?>" class="materialize-textarea"></textarea>
                                 <!-- <label for="textarea1">سلام</label> -->
                                <label for="<?php echo $index->value; ?>"><?php echo $index->name; ?></label>
                            </div>

                            <?php } elseif ($index->type === 'image') { ?>
                            <div class="file-field input-field">
                                <div style="height:40px !important;" class="btn main-bg black-text mt-2">
                                    <span>File</span>
                                    <input type="file" name="<?php echo $index->value; ?>" id="<?php echo $index->value; ?>" accept="image/*">
                                    <input type="file" name="<?php echo $index->value; ?>" id="<?php echo $index->value; ?>" accept="image/*">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                            <?php } ?>
                            
                            <?php if ($index->type !== 'text') { ?>
                                <label for="<?php echo $index->value; ?>"><?php echo $index->name; ?></label>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <button name="form-conclusion" id="form-conclusion" type="submit" class="waves-effect waves-light my-md-font btn-large yellow accent-4 black-text float-left m-3">ثبت</button>
            </form>
        </div>
        
    </section>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });

        $(".button-collapse").sideNav();

        function openNav() {
            document.getElementById("mySidenav").style.width = "200px";
            document.getElementById("main").style.marginLeft = "200px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
        }
    </script>
  </body>
</html>
