<?php
require_once 'DB.php';
$db = DB::getInstance();
if (isset($_POST['form-search'])) {
    $sql = "SELECT * FROM questions";
    $questions = $db->query($sql)->results();
    $index_sql = "SELECT * FROM indexes";
    $indexes = $db->query($index_sql)->results();
    $fields = [];
    $conditions = [];
    $binds = [];

    foreach ($questions as $q) {
        if(isset($_POST[$q->index_value])) {
          $fields[$q->index_value] = trim(htmlspecialchars($_POST[$q->index_value]));
          if ($fields[$q->index_value] != 0 && $fields[$q->index_value] != null) {
              $isql = "SELECT * FROM indexes WHERE value='$q->index_value'";
              $ind = $db->query($isql)->results();
              if ($ind[0]->type === 'multiple') {
                  $answer = $db->find('answers', [
                    'conditions' => "id = ?",
                    'bind' => [$fields[$q->index_value]]
                  ]);
                  $val = $answer[0]->value;
                  $condition = $q->index_value." LIKE '%$val%'";
                  array_push($conditions, $condition);
              } else {
                  $answer = $db->find('answers', [
                    'conditions' => "id = ?",
                    'bind' => [$fields[$q->index_value]]
                  ]);
                  $condition = $q->index_value." ".$answer[0]->operator." ?";
                  $bind = $answer[0]->value;
                  array_push($conditions, $condition);
                  array_push($binds, $bind);
              }
          }
        }
    }

    $conclusion = $db->find('conclusion', [
        'conditions' => $conditions,
        'bind' => $binds
    ]);
} else {
    header("location: index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="css/materialize-rtl.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <link rel="stylesheet" href="css/imagehover.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <title>Hoomat ES Builder - Results</title>

    <style>
        .row .col {
            float: right !important;
        }
        /* .poster {
          height: 200px !important
        } */
        .poster img {
          height: 300px !important
        }
    </style>
  </head>

  <body>

    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
          <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section class="container grey darken-4 mt-5 p-4 white-text">
      <div class="p-4">
        <div class="row">
          <div class="col s12">
            <?php
                foreach ($questions as $q) {
                  if(isset($_POST[$q->index_value])) {
                    $fields[$q->index_value] = trim(htmlspecialchars($_POST[$q->index_value]));
                    if ($fields[$q->index_value] != 0 && $fields[$q->index_value] != null) {
                      $answer = $db->find('answers', [
                        'conditions' => "id = ?",
                        'bind' => [$fields[$q->index_value]]
                      ]);
                      $index = $db->find('indexes', [
                        'conditions' => "value = ?",
                        'bind' => [$q->index_value]
                      ]);
                      $value = $answer[0]->title;
                      $name = $index[0]->name;
            ?>
              <span class="history-item"><b><?php echo $value; ?></b> <small>(<?php echo $name; ?>)</small>&nbsp&nbsp&nbsp/&nbsp&nbsp&nbsp</span>
            <?php
                    }
                  }
                }
            ?>
          </div>
        </div>
      </div>
    </section>

    <section class="container grey darken-4 mt-5 p-4">
      <div class="p-4">
        <div class="row">
        <?php if($conclusion){ foreach($conclusion as $res) {
                  if (isset($res->name)) {
                    $title = $res->name;
                  } elseif (isset($res->title)) {
                    $title = $res->title;
                  }

                  if (isset($res->image)) {
                    $image = $res->image;
                  } elseif (isset($res->poster)) {
                    $image = $res->poster;
                  } elseif (isset($res->featured_image)) {
                    $image = $res->featured_image;
                  } elseif (isset($res->cover)) {
                    $image = $res->cover;
                  }
        ?>
          <div class="col s6">
            <div class="card grey darken-3">
              <div class="card-image poster">
                <img style="object-fit:cover;" src="<?php echo $image; ?>">
                <span class="card-title"><?php echo $title; ?></span>
              </div>
              <div class="card-content white-text">
              
                <?php foreach($indexes as $index) { ?>
                  <?php if ($index->type !== 'image') { ?>
                    <div class="mt-3">
                      <span class="yellow-text"><?php
                        $value = $index->value;
                        $content = (is_null($res->$value)) ? '-----' : $res->$value;
                        echo $index->name." :";
                      ?></span>
                        <?php if ($index->type !== 'text') { ?>
                          <span class="float-left"><?php echo $content; ?></span>
                        <?php } else { ?>
                          <div class="my-desc"><?php echo $content; ?></div>
                        <?php } ?>
                    </div>
                  <?php } ?>
                <?php } ?>

                <div class="row mt-4">
                  <?php foreach($indexes as $index) { ?>
                    <?php if ($index->type === 'image') { ?>
                      <div class="col s4">
                        <span class="yellow-text"><?php
                          $value = $index->value;
                          $content = (is_null($res->$value)) ? '-----' : $res->$value;
                          $name = $index->name;
                        ?></span>
                        
                        <figure class="imghvr-shutter-in-out-diag-2">
                          <img src="<?php echo $content; ?>" class="my-image">
                          <figcaption class="my-sm-font">
                             <?php
                                $value = $index->value;
                                if ($value === 'male_actor_pic') {
                                  $val = "male_actor";
                                  $content = (is_null($res->$val)) ? '-----' : $res->$val;
                                  echo $name." : <div class='mt-3'>".$content."</div>";
                                } elseif ($value === 'female_actor_pic') {
                                  $val = "female_actor";
                                  $content = (is_null($res->$val)) ? '-----' : $res->$val;
                                  echo $name." : <div class='mt-3'>".$content."</div>";
                                } else {
                                  echo "پوستر فیلم";
                                }
                             ?>
                          </figcaption>
                          
                        </figure>
                      </div>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        <?php }} else { ?>
          <h4 class="red-text center-align">نتیجه ای برای نمایش وجود ندارد!</h4>
        <?php } ?>
        </div>
      </div>
    </section>

    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="https://cdn.rawgit.com/rstaib/jquery-steps/master/build/jquery.steps.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- <script src="js/app.js"></script> -->
    <script>
        $(document).ready(function(){
          $('.modal').modal();
        });

        $(".button-collapse").sideNav();

        function openNav() {
          document.getElementById("mySidenav").style.width = "200px";
          document.getElementById("main").style.marginLeft = "200px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("main").style.marginLeft= "0";
        }

        $(document).ready(function(){
          $('.tabs').tabs();
        });
    </script>
  </body>
</html>
