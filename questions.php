<?php
require_once 'DB.php';
$db = DB::getInstance();

$qsql = "SELECT * FROM questions";
$questions = $db->query($qsql)->results();
?>
<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="css/materialize-rtl.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>

    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
            <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section id="movie-info" class="container mt-5">
        <h5 class="white-text flashit mb-5">سوال ها</h5>
        <ul class="collection">
            <?php foreach ($questions as $q) { ?>
                <li class="collection-item my-md-font py-3 grey darken-4 main-color"><div><?php echo $q->title; ?><a href="#modal-<?php echo $q->id; ?>" class="secondary-content modal-trigger"><i class="main-color material-icons">add</i></a></div></li>

                <div id="modal-<?php echo $q->id; ?>" class="modal grey darken-4">
                    <div class="modal-content">
                        <div class="row">
                            <form action="add_answer.php" method="POST">
                                <div class="input-field col s12">
                                    <input id="title" name="title" type="text">
                                    <label for="title">عنوان جواب</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="operator" name="operator" type="text">
                                    <label for="operator">عملگر مقایسه</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="value" name="value" type="text">
                                    <label for="value">مقدار مقایسه</label>
                                </div>
                                <input type="hidden" value="<?php echo $q->id; ?>" name="question_id" id="question_id">

                                <button type="submit" name="form-answer" id="form-answer" class="waves-effect waves-light my-md-font btn yellow accent-4 black-text float-left m-3">ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </ul>
    </section>

    
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });

        $(".button-collapse").sideNav();

        function openNav() {
            document.getElementById("mySidenav").style.width = "200px";
            document.getElementById("main").style.marginLeft = "200px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
        }
    </script>
  </body>
</html>
