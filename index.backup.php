<?php
require_once 'DB.php';
$db = DB::getInstance();

$sql = "SELECT * FROM questions";
$questions = $db->query($sql)->results();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="css/materialize-rtl.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.6/select2-bootstrap.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.1/vue.min.js"></script>

    <title>Hoomat Builder - Search</title>
  </head>

<body>
  <div id="app">
    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
          <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section class="container grey darken-4 mt-5 p-4">
      <div class="p-4">
          <form id="example-form" action="search.php" method="POST">
            <div>
              <?php foreach ($questions as $q) { ?>
                <h3 class="my-md-font font-bld">
                  <?php
                    $index_sql = "SELECT * FROM indexes WHERE value='$q->index_value'";
                    $index = $db->query($index_sql)->results();
                    echo $index[0]->name;
                  ?>
                </h3>
                <section class="m-3 p-2 pt-5">
                    <div class="my-md-font mb-3 main-color"><?php echo $q->title; ?> :</div>
                    
                    <?php
                      $asql = "SELECT * FROM answers WHERE question_id='$q->id'";
                      $query = $db->query($asql);
                      $answers = $db->query($asql)->results();
                      if ($query->count() <= 6) { ?>
                        <?php foreach ($answers as $answer) { ?>
                        
                          <input value="<?php echo $answer->id; ?>" type="radio" name="<?php echo $q->index_value; ?>" id="<?php echo $q->index_value."-".$answer->id; ?>" />
                          <label class="white-text" for="<?php echo $q->index_value."-".$answer->id; ?>"><?php echo $answer->title; ?></label>
                        
                        <?php } ?>
                        </b-form-radio-group>
                        <input value="0" type="radio" name="<?php echo $q->index_value; ?>" id="<?php echo $q->index_value; ?>-0" />
                        <label class="white-text" for="<?php echo $q->index_value; ?>-0">نادیده گرفتن</label>

                      <?php } else { ?>
                        <single-select inline-template>
                          <div class="container">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <select-2 class="text-right" :options="<?php echo $q->index_value."_options"; ?>" name="test" v-model="<?php echo $q->index_value; ?>"></select-2>
                                  </div>
                              </div>
                          </div>
                        </single-select>
                      <?php } ?>
                </section>
              <?php } ?>
            </div>
            <button type="submit" id="form-search" name="form-search" style="display: none;"></button>
          </form>
      </div>
    </section>

  </div>
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="https://cdn.rawgit.com/rstaib/jquery-steps/master/build/jquery.steps.min.js"></script>
    <script src="js/app.js"></script>
    <script>
        Vue.component('select-2', {
          template: '<select v-bind:name="name" class="form-control"></select>',
          props: {
              name: '',
              options: {
                  Object
              },
              value: null,
          },
          data() {
              return {
                  select2data: []
              }
          },
          mounted() {
              this.formatOptions()
              let vm = this
              let select = $(this.$el)
              select
                  .select2({
                      placeholder: 'Select',
                      theme: 'bootstrap',
                      width: '100%',
                      allowClear: true,
                      data: this.select2data
                  })
                  .on('change', function () {
                      vm.$emit('input', select.val())
                  })
              select.val(this.value).trigger('change')
          },
          methods: {
              formatOptions() {
                  this.select2data.push({ id: '', text: 'انتخاب کنید...' })
                  for (let key in this.options) {
                      this.select2data.push({ id: key, text: this.options[key] })
                  }
              }
          },
          destroyed: function () {
              $(this.$el).off().select2('destroy')
          }
      })

      
      <?php
          foreach($questions as $q) {
            $text = $q->index_value."_options";
            echo "const $text = {";
            $asql = "SELECT * FROM answers WHERE question_id='2'";
            $query = $db->query($asql);
            $answers = $db->query($asql)->results();
            foreach($answers as $ans) {
              echo "$ans->id: '$ans->title',";
            }
            echo "}\n";
          }
      ?>

      const singleSelect = Vue.component('single-select', {
          data() {
              return {
                  <?php
                    foreach($questions as $q) {
                      $text = $q->index_value.": null,\n";
                      echo $text;
                    }
                    foreach($questions as $q) {
                      $text = $q->index_value."_options,\n";
                      echo $text;
                    }
                  ?>
              }
          }
      })

      const app = new Vue({
          el: '#app',
      })
    </script>

  </body>
</html>