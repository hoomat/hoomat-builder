<?php
require_once 'DB.php';
$db = DB::getInstance();
if (isset($_POST['form-question'])) {
    $name = trim(htmlspecialchars($_POST['name']));
    $value = trim(htmlspecialchars($_POST['value']));
    $type = trim(htmlspecialchars($_POST['type']));
    $question = trim(htmlspecialchars($_POST['question']));

    if (empty($name) || empty($value) || empty($type) || empty($question)) {
        header("location: create_question.html");
    } else {
        $sql = "INSERT INTO indexes (name, value, type) VALUES ('$name', '$value', '$type')";
        $db->query($sql);

        if ($type !== 'image') {
            $qsql = "INSERT INTO questions (title, index_value) VALUES ('$question', '$value')";
            $db->query($qsql);
        }

        if ($type === 'varchar' || $type === 'multiple' || $type === 'image') {
            $sql = "ALTER TABLE conclusion ADD $value VARCHAR( 255 )";
            $db->query($sql);
        } elseif ($type === 'integer') {
            $sql = "ALTER TABLE conclusion ADD $value INT( 11 )";
            $db->query($sql);
        } elseif ($type === 'text') {
            $sql = "ALTER TABLE conclusion ADD $value TEXT";
            $db->query($sql);
        } elseif ($type === 'float') {
            $sql = "ALTER TABLE conclusion ADD $value FLOAT";
            $db->query($sql);
        }
    }
    header("location: create_question.php");
} else {
    header("location: create_question.php");
}