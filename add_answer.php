<?php
require_once 'DB.php';
$db = DB::getInstance();
if (isset($_POST['form-answer'])) {
    $title = trim(htmlspecialchars($_POST['title']));
    $value = trim(htmlspecialchars($_POST['value']));
    $operator = trim($_POST['operator']);
    $question_id = $_POST['question_id'];

    if (empty($title) || empty($value) || empty($operator) || empty($question_id)) {
        header("location: questions.php");
    } else {
        $sql = "INSERT INTO answers (title, value, operator, question_id) VALUES ('$title', '$value', '$operator', '$question_id')";
        $db->query($sql);
        header("location: questions.php");
    }
    die();
} else {
    header("location: questions.php");
}