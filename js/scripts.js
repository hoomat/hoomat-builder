'use strict';

$(window).on('load', function() { 
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut(); 
	$("#preloder").delay(400).fadeOut("slow");

	window.setTimeout(function () {
		window.location.href = "index.php";
	}, 12500);
	/*------------------
	Isotope Filter
	--------------------*/
	var $container = $('.isotope_items');
	$container.isotope();

	$('.portfolio-filter li').on("click", function(){
		$(".portfolio-filter li").removeClass("active");
		$(this).addClass("active");				 
		var selector = $(this).attr('data-filter');
		$(".isotope_items").isotope({
				filter: selector,
				animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false,
			}
		});
		return false;
	});

});


(function($){

	/*------------------
		Navigation
	--------------------*/
	$('.responsive-switch').on('click', function(e) {
		$('.site-menu').toggleClass('active');	
		e.preventDefault();
	});

	$('.menu-list>li>a, .sm-close').on('click', function() {
		$('.site-menu').removeClass('active');
	});

	$('.menu-list').onePageNav({
		easing: 'swing'
	});


	/*------------------
		Hero section
	--------------------*/
	var hero_h = $('.hero-section').innerHeight(),
		body_h = $('body').innerHeight(),
		header_height =  hero_h - body_h;

	$(window).on('scroll resize',function(e) {
		if ($(this).scrollTop() > header_height) {
			$('.hero-content').addClass('sticky');
		}else{
			$('.hero-content').removeClass('sticky');
		}
		e.preventDefault();
	});



	/*------------------
		Typed js
	--------------------*/
	if($('#typed-text').length > 0 ) {
		var typed2 = new Typed('#typed-text', {
			strings: ["Alireza Rahimi", "Ali Golmehr", "Milad Shahi"],
			typeSpeed: 50,
			loop:true,
			backDelay: 3500
		});
	}



	/*------------------
		Background set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});

	/*------------------
		WOW JS
	--------------------*/
	new WOW().init();


	/*------------------
		CONTACT FORM
	--------------------*/
	$('#contact-form').on('submit', function() {
		var send_btn = $('#send-form'),
			form = $(this),
			formdata = $(this).serialize(),
			chack = $('#form-chack');
			send_btn.text('Wait...');

		function reset_form(){
		 	$("#name").val('');
			$("#email").val('');
			$("#massage").val('');
		}

		$.ajax({
			url:  $(form).attr('action'),
			type: 'POST',
			data: formdata,
			success : function(text){
				if (text == "success"){
					send_btn.addClass('done');
					send_btn.text('Success');
					setTimeout(function() {
						reset_form();
						send_btn.removeClass('done');
						send_btn.text('Send Massage');
					}, 3000);
				}
				else {
					reset_form();
					send_btn.addClass('error');
					send_btn.text('Error');
					setTimeout(function() {
						send_btn.removeClass('error');
						send_btn.text('Send Massage');
					}, 5000);
				}
			}
		});
		return false;
	});

})(jQuery);