$(document).ready(function () {
    $('.tabs').tabs();
    $('.modal').modal();
    // $('.select1').sm_select({
    //     show_placeholder: true,
    //     duration: 600,
    //     input_text: 'Custom placeholder...'
    // });
    $('.select1').select2({
        placeholder: 'Select an option',
        theme: "material"
    });
    $(".select2-selection__arrow")
        .addClass("material-icons")
        .html("arrow_drop_down");
})
var form = $("#example-form");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",

    /* Labels */
    labels: {
        cancel: "انصراف؟",
        current: "current step:",
        pagination: "Pagination",
        finish: "پایان",
        next: "بعدی",
        previous: "قبلی",
        loading: "در حال بارگذاری"
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        return true;
    },
    onFinishing: function (event, currentIndex) {
        return true;
    },
    onFinished: function (event, currentIndex) {
        var button = $("#form-search");
        button.click();
    }
});

$(".button-collapse").sideNav();

function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementById("main").style.marginLeft = "200px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}