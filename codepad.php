<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!-- <link rel="stylesheet" href="css/materialize-rtl.css"> -->
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-4-utilities.css">
    <link rel="stylesheet" href="http://codemirror.net/lib/codemirror.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Hoomat ES Builder - Add New Question</title>
  </head>

  <body>

    <!-- Start Sidenav -->
        <nav class="grey darken-4">
            <span style="cursor:pointer;" class="float-right mr-3 mt-1" onclick="openNav()"><i class="large material-icons">menu</i></span>
            <a href="index.php"><span class="float-left px-3 my-md-font main-bg black-text">جستجوی فیلم</span></a>
        </nav>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn ml-2" onclick="closeNav()">&times;</a>
            <a href="create_conclusion.php" class="p-3 mt-5 sidenav-item">افزودن نتیجه</a>
            <a href="create_question.php" class="p-3 sidenav-item">افزودن سوال</a>
            <a href="questions.php" class="p-3 sidenav-item">سوال ها</a>
        </div>
    <!-- End Sidenav -->

    <section id="movie-info" class="mt-5">
        <!-- <h5 class="white-text flashit mb-5">افزودن سوال</h5> -->
        <div class="row">
            <div class="input-field col s6">
                <textarea rows="8" id="html_editor"></textarea>
                <!-- <label for="html_editor">HTML</label> -->
            </div>
            <div class="input-field col s6">
                <textarea rows="8" id="css_editor"></textarea>
                <!-- <label for="css_editor">CSS</label> -->
            </div>
            <div class="input-field col s6">
                <textarea rows="8" id="js_editor"></textarea>
                <!-- <label for="js_editor">JS</label> -->
            </div>
            <div class="col s6">
                <iframe id="preview" style="width: 100%; height: 400px; background: #fff;"></iframe>
            </div>
        </div>
        
    </section>

            
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="http://codemirror.net/lib/codemirror.js"></script>
    <!-- For HTML/XML -->
    <script src="http://codemirror.net/mode/xml/xml.js"></script>
    <script src="http://codemirror.net/mode/htmlmixed/htmlmixed.js"></script>
    <!-- For CSS -->
    <script src="http://codemirror.net/mode/css/css.js"></script>
    <!-- For JS -->
    <script src="http://codemirror.net/mode/javascript/javascript.js"></script>

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
        $(document).ready(function(){
            $('.modal').modal();
        });
        $(".button-collapse").sideNav();
        function openNav() {
            document.getElementById("mySidenav").style.width = "200px";
            document.getElementById("main").style.marginLeft = "200px";
        }
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
        }

        var html_editor = document.querySelector('#html_editor'),
            css_editor = document.querySelector('#css_editor'),
            js_editor = document.querySelector('#js_editor');

        var editors = [html_editor, css_editor, js_editor];
        // Attaching the onkeyup Event
        editors.forEach(function(editor, i, arr) {
            editor.addEventListener('keyup', function() {
                // The function that'll prepare the code and inject
                // into the iframe.
                render();
            }, false);
        });

        (function() {
            
            // Base template
            var base_tpl =
                "<!doctype html>\n" +
                "<html>\n\t" +
                "<head>\n\t\t" +
                "<meta charset=\"utf-8\">\n\t\t" +
                "<title>Test</title>\n\n\t\t\n\t" +
                "</head>\n\t" +
                "<body>\n\t\n\t" +
                "</body>\n" +
                "</html>";
            
            var prepareSource = function() {
                var html = html_editor.getValue(),
                    css = css_editor.getValue(),
                    js = js_editor.getValue(),
                    src = '';
                
                // HTML
                src = base_tpl.replace('</body>', html + '</body>');
                
                // CSS
                css = '<style>' + css + '</style>';
                src = src.replace('</head>', css + '</head>');
                
                // Javascript
                js = '<script>' + js + '<\/script>';
                src = src.replace('</body>', js + '</body>');
                
                return src;
            };
            
            function render() {
                var source = prepareSource();
                
                var iframe = document.querySelector('#preview'),
                    iframe_doc = iframe.contentDocument;
                
                iframe_doc.open();
                iframe_doc.write(source);
                iframe_doc.close();
            };
            
            
            // EDITORS
            
            // CM OPTIONS
            var cm_opt = {
                mode: 'text/html',
                gutter: true,
                lineNumbers: true,
            };
            
            // HTML EDITOR
            var html_box = document.querySelector('#html_editor');
            var html_editor = CodeMirror.fromTextArea(html_box, cm_opt);
        
            html_editor.on('change', function (inst, changes) {
                render();
            });
            
            // CSS EDITOR
            cm_opt.mode = 'css';
            var css_box = document.querySelector('#css_editor');
            var css_editor = CodeMirror.fromTextArea(css_box, cm_opt);
        
            css_editor.on('change', function (inst, changes) {
                render();
            });
            
            // JAVASCRIPT EDITOR
            cm_opt.mode = 'javascript';
            var js_box = document.querySelector('#js_editor');
            var js_editor = CodeMirror.fromTextArea(js_box, cm_opt);
        
            js_editor.on('change', function (inst, changes) {
                render();
            });
            
            // SETTING CODE EDITORS INITIAL CONTENT
            html_editor.setValue('<p>Hello World</p>');
            css_editor.setValue('body { color: red; }');
            
            
            // RENDER CALL ON PAGE LOAD
            // NOT NEEDED ANYMORE, SINCE WE RELY
            // ON CODEMIRROR'S onChange OPTION THAT GETS
            // TRIGGERED ON setValue
            // render();
            
            
            // NOT SO IMPORTANT - IF YOU NEED TO DO THIS
            // THEN THIS SHOULD GO TO CSS
            
            /*
                Fixing the Height of CodeMirror.
                You might want to do this in CSS instead
                of JS and override the styles from the main
                codemirror.css
            */
            // var cms = document.querySelectorAll('.CodeMirror');
            // for (var i = 0; i < cms.length; i++) {
                
            //     cms[i].style.position = 'absolute';
            //     cms[i].style.top = '30px';
            //     cms[i].style.bottom = '0';
            //     cms[i].style.left = '0';
            //     cms[i].style.right = '0';
            //     cms[i].style.height = '100%';
            // }
            /*cms = document.querySelectorAll('.CodeMirror-scroll');
            for (i = 0; i < cms.length; i++) {
                cms[i].style.height = '100%';
            }*/
                    
        }());
    </script>
  </body>
</html>
